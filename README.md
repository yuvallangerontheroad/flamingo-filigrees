# Flamingo Filigrees

## Mirrors:

- Main on CodeBerg:
    - https://codeberg.org/yuvallangerontheroad/flamingo-filigrees/
- Secondary on GitLab:
    - https://gitlab.com/yuvallangerontheroad/flamingo-filigrees/

## Messages:

### color

client would send at each change of color:

```JSON
{
        "type": "color",
        "value": color
}
```

the value of `color` a string representing a three or six digit hexadecimal number, e.g. `"ddd"` or `"80461B"`.

### width

client would send at each change of width:

```JSON
{
        "type": "width",
        "value": width,
}
```

width is a number representing the width of the line.

### path

client would send at each touchEnd or mouseUp(???) the following message:

```JSON
{
        "type": "path",
        "value": path,
}
```

`path` is an Array of Arrays of pairs of numbers, each pair representing the pageX and pageY values of a single point along the path.
