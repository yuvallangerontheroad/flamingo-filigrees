# Changelog

## v0.1.0 - 2021-05-01

First commit. Yay.

* Users can draw in the same canvas.
* Nicknames are loaded from a JSON file, or if the file is missing, are generated as unique random strings.
* Add a README file.
    * Add links to mirrors.
* Add a CHANGELOG
* Flamingo Filigrees is under the AGPL-3.0-or-later license.
