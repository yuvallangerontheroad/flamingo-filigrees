# A word guessing game for the web.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
import json
import logging
import random
import string

import websockets


USERS = dict()
POSSIBLE_NICKNAMES = None


def load_possible_nicknames(filename):
    global POSSIBLE_NICKNAMES
    try:
        with open(filename, 'r') as f:
            POSSIBLE_NICKNAMES = json.loads(f.read())['names']
            logging.debug(POSSIBLE_NICKNAMES)
    except FileNotFoundError:
        pass


def make_random_nickname():
    if POSSIBLE_NICKNAMES:
        nickname = random.choice(POSSIBLE_NICKNAMES)
    else:
        while True:
            nickname = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
            if nickname not in USERS.values():
                break
    return nickname


async def register(my_websocket):
    USERS[my_websocket] = make_random_nickname()
    structured_message = {'type': 'register', 'data': USERS[my_websocket]}
    raw_message = json.dumps(structured_message)
    for other_websocket in USERS:
        await other_websocket.send(raw_message)
    print(f'Registered {my_websocket} as {USERS[my_websocket]}.')


async def unregister(my_websocket):
    print(f'Unregistering {my_websocket} ({USERS[my_websocket]})')
    deleted_nickname = USERS[my_websocket]
    message = {'type': 'unregister', 'data': deleted_nickname}
    raw_message = json.dumps(message)
    del USERS[my_websocket]
    for other_websocket in USERS:
        await other_websocket.send(raw_message)
    print(f'Unregistered {deleted_nickname}.')


async def server(my_websocket, path):
    await register(my_websocket)
    try:
        async for raw_message in my_websocket:

            structured_message = json.loads(raw_message)

            if structured_message['type'] in ['path', 'lineWidth', 'strokeStyle']:
                for other_websocket in USERS:
                    if other_websocket != my_websocket:
                        await other_websocket.send(raw_message)
    finally:
        await unregister(my_websocket)


def main():
    logging.basicConfig(level=logging.DEBUG)
    load_possible_nicknames('../data/names.json')

    asyncio.get_event_loop().run_until_complete(
        websockets.serve(server, '127.0.0.1', '20001')
    )
    asyncio.get_event_loop().run_forever()


if __name__ == '__main__':
    main()
