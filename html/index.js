/* A word guessing game for the web.
 * Copyright (C) 2021  Yuval Langer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

let DEBUG = true;

Math.TAU = 2 * Math.PI;


window.addEventListener('load', function() {
	let WEBSOCKET = new WebSocket('ws://127.0.0.1:20001');

	let ONGOING_TOUCHES = [];

	let PATHS = [];
	let STROKE_STYLE = '#000';
	let LINE_WIDTH = 4;


	function prelogln(s) {
		if (DEBUG) {
			let log_element = document.getElementById('log');
			log_element.innerText += `${Date()}: ${s}\n`;
		};
	};


	function copyTouch({ identifier, pageX, pageY }) {
		return { identifier, pageX, pageY };
	};


	function ongoingTouchIndexById(idToFind) {
		for (let i in ONGOING_TOUCHES) {
			let id = ONGOING_TOUCHES[i].identifier;

			if (id == idToFind) {
				return i;
			};
		};
	};


	function startHandler(evt) {
		evt.preventDefault();
		let el = document.getElementById('canvas');
		let ctx = el.getContext('2d');
		let touches = evt.changedTouches;

		for (let i = 0; i < touches.length; i++) {
			ONGOING_TOUCHES.push(copyTouch(touches[i]));
			ctx.beginPath();
			ctx.arc(touches[i].pageX, touches[i].pageY, 4, 0, Math.TAU, false);
			ctx.fillStyle = STROKE_STYLE;
			ctx.fill();
			let new_path = [[touches[i].pageX, touches[i].pageY]];
			PATHS[touches[i].identifier] = new_path;
		};
	};
	function endHandler(evt) {
		evt.preventDefault();
		let el = document.getElementById('canvas');
		let ctx = el.getContext('2d');
		let touches = evt.changedTouches;

		for (let i = 0; i < touches.length; i++) {
			let idx = ongoingTouchIndexById(touches[i].identifier);

			if (idx >= 0) {
				ctx.lineWidth = 4;
				ctx.fillStyle = STROKE_STYLE;
				ctx.beginPath();
				ctx.moveTo(ONGOING_TOUCHES[idx].pageX, ONGOING_TOUCHES[idx].pageY);
				ctx.lineTo(touches[i].pageX, touches[i].pageY);
				ctx.fillRect(touches[i].pageX - 4, touches[i].pageY - 4, 8, 8);

				ONGOING_TOUCHES.splice(idx, 1);
			} else {	
				prelogln("Can't figure out which touch to end.");
			};
		};
	};
	function cancelHandler(evt) {
		evt.preventDefault();
		let touches = evt.changedTouches;

		for (let i = 0; i < touches.length; i++) {
			let idx = ongoingTouchIndexById(touches[i].identifier);
			ONGOING_TOUCHES.splice(idx, 1);
		};
	};
	function moveHandler(evt) {
		evt.preventDefault();
		let el = document.getElementById('canvas');
		let ctx = el.getContext('2d');
		let touches = evt.changedTouches;

		for (let i = 0; i < touches.length; i++) {
			let idx = ongoingTouchIndexById(touches[i].identifier);

			if (idx >= 0) {
				ctx.beginPath();
				ctx.moveTo(ONGOING_TOUCHES[idx].pageX, ONGOING_TOUCHES[idx].pageY);
				ctx.lineTo(touches[i].pageX, touches[i].pageY);
				ctx.lineWidth = LINE_WIDTH;
				ctx.strokeStyle = STROKE_STYLE;
				ctx.stroke();

				ONGOING_TOUCHES.splice(idx, 1, copyTouch(touches[i]));

				let current_xy = [touches[i].pageX, touches[i].pageY]
				PATHS[touches[i].identifier].push(current_xy);
			} else {
				prelogln("Can't figure out which touch to continue.");
			};
		};
	};


	function blankCanvas() {
		let el = document.getElementById('canvas');
		let ctx = el.getContext('2d');

		ctx.fillStyle = 'white';
		ctx.fillRect(0, 0, canvas.width, canvas.height);
	};


	function drawLine(path, strokeStyle, lineWidth) {
		let el = document.getElementById('canvas');
		let ctx = el.getContext('2d');

		ctx.beginPath();
		ctx.moveTo(path[0][0], path[0][1]);

		for (
			let coordinate_i = 1;
			coordinate_i < path.length;
			coordinate_i++
		) {
			let pageX = path[coordinate_i][0];
			let pageY = path[coordinate_i][1];

			ctx.lineTo(pageX, pageY);
		};

		ctx.lineWidth = lineWidth;
		ctx.strokeStyle = strokeStyle;
		ctx.stroke();
	};


	function websocketMessageHandler(evt) {
		prelogln(evt.data);

		let structured_data = JSON.parse(evt.data);

		switch (structured_data.type) {
			case 'register':
				prelogln(`Registered "${structured_data.value}".`)
				break;
			case 'unregister':
				prelogln(`Unregistered "${structured_data.value}".`);
				break;
			case 'path':
				prelogln(`${STROKE_STYLE} ${LINE_WIDTH} ${structured_data.value}`);
				drawLine(structured_data.value, STROKE_STYLE, LINE_WIDTH);
				break;
			case 'lineWidth':
				LINE_WIDTH = structured_data.value;
				break;
			case 'strokeStyle':
				STROKE_STYLE = structured_data.value;
				break;
			default:
				prelogln(`Unsupported event: ${evt.data}`);
		};
	};


	function sendStrokeStyle() {
		let message_object = {
			'type': 'strokeStyle',
			'value': STROKE_STYLE,
		};
		let stringified_message = JSON.stringify(message_object);
		WEBSOCKET.send(stringified_message);
	};

	function sendStrokeStyle() {
		let message_object = {
			'type': 'lineWidth',
			'value': LINE_WIDTH,
		};
		let stringified_message = JSON.stringify(message_object);
		WEBSOCKET.send(stringified_message);
	};
	function sendLines(evt) {
		evt.preventDefault();
		touches = evt.changedTouches;

		for (let i = 0; i < touches.length; i++) {
			let path = PATHS[touches[i].identifier];
			message_object = {
				'type': 'path',
				'value': path,
			};
			let stringified_message = JSON.stringify(message_object);
			WEBSOCKET.send(stringified_message);

			delete PATHS[touches[i].identifier];
		};
	};

	let canvas_element = document.getElementById('canvas');
	canvas_element.addEventListener('touchstart', startHandler, false);
	canvas_element.addEventListener('touchend', endHandler, false);
	canvas_element.addEventListener('touchcancel', cancelHandler, false);
	canvas_element.addEventListener('touchmove', moveHandler, false);

	canvas_element.addEventListener('touchend', sendLines, false);
	canvas_element.addEventListener('touchcancel', sendLines, false);

	WEBSOCKET.addEventListener('message', websocketMessageHandler);

	blankCanvas();
});
